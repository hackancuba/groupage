import json
import subprocess
from pathlib import Path
from secrets import token_hex
from typing import Any, Annotated, Sequence
from typing import TypedDict, Optional

import typer

MAIN_DIR = Path.home() / Path(".groupage")
IDS_DIR = MAIN_DIR / Path('ids')
REPOSITORIES_DIR = MAIN_DIR / Path('repos')
PROFILES_PATH = MAIN_DIR / Path('profiles.json')
AGE_SECKEY_EXTENSION = 'key'
AGE_PUBKEY_EXTENSION = 'pub'

app = typer.Typer(help='CLI tool to ease the usage of age for small groups')


class Profile(TypedDict):
    repo: Path
    name: str
    id: str


class CustomJSONEncoder(json.JSONEncoder):

    def default(self, o: Any) -> Any:
        if isinstance(o, Path):
            return str(o)

        return super().default(o)


def age_create_identity(path: Path) -> None:
    # ToDo: use [pyrage](https://github.com/woodruffw/pyrage) ?
    subprocess.run(["age-keygen", "-o", path], check=True)
    typer.echo('New age key created')


def age_get_public_key_from_secret_key(path: Path) -> str:
    proc = subprocess.run(["age-keygen", "-y", path], check=True, capture_output=True)
    pub_key = proc.stdout.decode().strip()

    return pub_key


def age_get_public_key(path: Path) -> str:
    return path.read_text().strip()


def age_encrypt(
    *,
    recipient: Sequence[str],
    armor: bool,
    output: Path | None,
    input_: Path | None,
) -> None:
    command = ["age", "--encrypt"]

    if armor:
        command.append('--armor')

    if output:
        command.append('--output')
        command.append(output)

    for pubkey in recipient:
        command.append('--recipient')
        command.append(pubkey)

    # ToDo: pass stdin to the process
    if input_:
        command.append(input_)

    subprocess.run(command, check=True)


def age_decrypt(*, identity: Path, output: Path | None, input_: Path | None) -> None:
    command = ['age', '--decrypt', '--identity', identity]

    if output:
        command.append('--output')
        command.append(output)

    # ToDo: pass stdin to the process
    if input_:
        command.append(input_)

    subprocess.run(command, check=True)


def git_get_name() -> str:
    options = ('user.name', 'user.email')
    for option in options:
        proc = subprocess.run(['git', 'config', '--get', option], check=True, capture_output=True)
        name = proc.stdout.decode().strip()
        if name:
            return name

    raise RuntimeError('could not get name from git config')


def git_refresh(path: Path) -> None:
    subprocess.run(['git', 'pull', '--prune'], check=True, cwd=path)
    typer.echo('Git repo refreshed')


def git_do_everything(*, path: Path, public_key_content: str, name: str, uri: str) -> Path:
    # clone repo, unless it exists already
    # pull
    # check for paths in repo, create necessary ones
    # write PK in file {name}.pub
    # if the file exists, panic
    # commit changes and push
    # ToDo: consider https://github.com/gitpython-developers/GitPython, although its barely maintained
    repo_dir = uri.split('/')[-1].removesuffix('.git')  # assuming SSH URI!
    repo_path = path / Path(repo_dir)

    if repo_path.exists():
        git_refresh(repo_path)
    else:
        subprocess.run(['git', 'clone', uri], check=True, cwd=path)
        typer.echo(f'Git repo cloned from "{uri}')

    public_keys_dir = repo_path / Path('.groupage')
    public_keys_dir.mkdir(exist_ok=True)
    public_key_path = public_keys_dir / Path(name + '.' + AGE_PUBKEY_EXTENSION)
    if public_key_path.exists():
        return repo_path

    with open(public_key_path, 'wt') as file:
        file.write(public_key_content)

    subprocess.run(['git', 'add', public_key_path], check=True, cwd=repo_path)
    subprocess.run(
        ['git', 'commit', '-m', f'[groupage] Add public key for {name}'],
        check=True,
        cwd=repo_path,
    )
    subprocess.run(['git', 'push', 'origin', 'HEAD'], check=True, cwd=repo_path)

    return repo_path


def profile_read_default(path: Path) -> Profile:
    with path.open('rt') as file:
        raw_profiles = json.load(file)

    return Profile(**raw_profiles[0])


@app.command()
def init(
    git_uri: str,
    identity: Annotated[str, typer.Option('--identity', '-i')] = '',
    name: Annotated[str, typer.Option()] = '',
) -> None:
    # create ~/.groupage (if it doesn't exist)
    # create required dirs (if they don't exist): repos and ids
    # create profiles.json (if it doesn't exist, exit with error otherwise; provide option to create new profile)
    # create new key (unless an identity is specified, in which case copy it)
    # add new profile
    # clone repo
    # add new pubkey (if it doesn't exist), commit and push
    # ToDo: resolve() paths, use functions to get them?
    MAIN_DIR.mkdir(exist_ok=True)
    IDS_DIR.mkdir(exist_ok=True)
    REPOSITORIES_DIR.mkdir(exist_ok=True)

    if PROFILES_PATH.exists():
        raise RuntimeError('groupage seems initialized already')

    if not name.strip():
        name = git_get_name()

    name = name.lower()
    typer.echo(f'Initializing for "{name}"')

    id_path = IDS_DIR / Path(name + '.' + AGE_SECKEY_EXTENSION)
    if identity:
        identity_path = Path(identity.strip())
        assert identity_path.exists()
        identity_path.hardlink_to(id_path)
    elif not id_path.exists():
        age_create_identity(id_path)

    public_key_content = age_get_public_key_from_secret_key(id_path)
    repo = git_do_everything(
        path=REPOSITORIES_DIR,
        public_key_content=public_key_content,
        name=name,
        uri=git_uri.strip(),
    )

    profiles = [
        Profile(
            repo=repo.relative_to(REPOSITORIES_DIR),
            name=name,
            id=token_hex(4),
        ),
    ]
    with PROFILES_PATH.open('wt') as file:
        json.dump(profiles, file, cls=CustomJSONEncoder, indent=2)
    typer.echo('New profile created')
    typer.echo('All done!')


@app.command()
def encrypt(
    recipient: Annotated[
        list[str],
        typer.Option('--recipient', '-r', show_default=False),
    ],
    input_: Annotated[Optional[str], typer.Argument(metavar='INPUT')] = None,
    armor: Annotated[bool, typer.Option('--armor', '-a')] = False,
    output: Annotated[str, typer.Option('--output', '-o')] = '',
) -> None:
    profile = profile_read_default(PROFILES_PATH)
    repo = REPOSITORIES_DIR / Path(profile['repo'])
    git_refresh(repo)

    pubkeys_dir = repo / Path('.groupage')
    pubkeys = [
        age_get_public_key(pubkeys_dir / Path(name + '.' + AGE_PUBKEY_EXTENSION)) for name in recipient
    ]
    age_encrypt(
        recipient=pubkeys,
        armor=armor,
        output=Path(output) if output else None,
        input_=input_,
    )


@app.command()
def decrypt(
    input_: Annotated[Optional[str], typer.Argument(metavar='INPUT')] = None,
    output: Annotated[str, typer.Option('--output', '-o')] = '',
) -> None:
    profile = profile_read_default(PROFILES_PATH)
    identity = IDS_DIR / Path(profile['name'] + '.' + AGE_SECKEY_EXTENSION)
    age_decrypt(identity=identity, output=Path(output) if output else None, input_=input_)


if __name__ == "__main__":
    app()
