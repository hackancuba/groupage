"""Common tasks for Invoke."""

import codecs
import os
import typing
from contextlib import contextmanager
from datetime import datetime
from datetime import timezone
from functools import partial
from pathlib import Path
from tempfile import mkstemp
from unittest.mock import patch

from invoke import Context
from invoke import Exit
from invoke import Result
from invoke import task
from junitparser import JUnitXml


@task
def flake8(ctx):
    """Run flake8 with proper exclusions."""
    ctx.run('flake8 --exclude tests groupage/', echo=True)
    ctx.run('flake8 --ignore=S101,R701,C901 groupage/tests/', echo=True)
    ctx.run('flake8 tasks.py', echo=True)


@task
def pydocstyle(ctx):
    """Run pydocstyle with proper exclusions."""
    ctx.run('pydocstyle --explain groupage/', echo=True)
    ctx.run('pydocstyle --explain tasks.py', echo=True)


@task
def darglint(ctx):
    """Run darglint."""
    ctx.run('darglint -v2 groupage/', echo=True)
    ctx.run('darglint -v2 tasks.py', echo=True)


@task
def bandit(ctx):
    """Run bandit with proper exclusions."""
    ctx.run('bandit -i -r -x groupage/tests groupage/', echo=True)
    ctx.run('bandit -i -r -s B101 groupage/tests/', echo=True)
    ctx.run('bandit -i -r tasks.py', echo=True)


@task
def mypy(ctx):
    """Lint code with mypy."""
    ctx.run('mypy groupage/', echo=True, pty=True)


@task
def yapf(ctx, diff=False):
    """Run yapf to format the code."""
    cmd = ['yapf', '--recursive', '--verbose', '--parallel']
    if diff:
        cmd.append('-d')
    else:
        cmd.append('-i')

    cmd.append('groupage/')
    cmd.append('tasks.py')

    ctx.run(' '.join(cmd), echo=True)


@task
def trailing_commas(ctx):
    """Add missing trailing commas, or remove them if necessary."""
    opts = r'-type f -name "*.py" -exec add-trailing-comma "{}" \+'  # noqa: P103
    ctx.run('find groupage/ ' + opts, echo=True, pty=True, warn=True)
    ctx.run('add-trailing-comma tasks.py', echo=True, pty=True, warn=True)


# noinspection PyUnusedLocal
@task(yapf, trailing_commas)
def reformat(ctx):  # pylint: disable=W0613
    """Reformat code (runs YAPF and add-trailing-comma)."""


@task
def pylint(ctx):
    """Run pylint."""
    ctx.run('pylint groupage/ --ignore tests', echo=True, pty=True, warn=True)
    ctx.run('pylint groupage/tests/ --exit-zero', echo=True, pty=True, warn=True)
    ctx.run('pylint tasks.py --exit-zero', echo=True, pty=True, warn=True)


# noinspection PyUnusedLocal
@task(flake8, pylint, pydocstyle, darglint, mypy, bandit)
def lint(ctx):  # pylint: disable=W0613
    """Lint code, and run static analysis.

    Runs flake8, pylint, pydocstyle, darglint, mypy, and bandit.
    """


@task
def clean(ctx):
    """Remove all temporary and compiled files."""
    remove = (
        'build',
        'dist',
        '*.egg-info',
        '.coverage',
        'cover',
        'htmlcov',
        '.mypy_cache',
        '.pytest_cache',
        'site',
    )
    ctx.run(f'rm -vrf {" ".join(remove)}', echo=True)
    ctx.run(r'find . -type d -name "__pycache__" -exec rm -rf "{}" \+', echo=True)  # noqa: P103
    ctx.run('find . -type f -name "*.pyc" -delete', echo=True)


@task(
    aliases=['test'],
    help={
        'watch': 'run tests continuously with pytest-watch',
        'seed': 'seed number to repeat a randomization sequence',
        'coverage': 'run with coverage (or not)',
        'report': 'produce a JUnit XML report file as "report.xml" (requires coverage)',
    },
)
def tests(ctx, watch=False, seed=0, coverage=True, report=False):  # noqa: C901,R701
    """Run tests."""
    junit_report = report and coverage

    if watch:
        cmd = ['pytest-watch', '--']
    else:
        cmd = ['pytest', '--suppress-no-test-exit-code']

    if seed:
        cmd.append(f'--randomly-seed="{seed}"')

    if not coverage:
        cmd.append('--no-cov')

    if junit_report:
        cmd.append('--junitxml=report.xml')

    if coverage:
        cmd.append('--cov-append')

    cmd.append('groupage')

    ctx.run(' '.join(cmd0), pty=True, echo=True)


@task
def safety(ctx):
    """Run Safety dependency vuln checker."""
    print('Safety check project requirements...')
    fd, requirements_path = mkstemp(prefix='gpa')
    os.close(fd)
    try:
        ctx.run(f'poetry export -f requirements.txt -o "{requirements_path}" --group dev')
        ctx.run(f'safety check --full-report -r "{requirements_path}"')
    finally:
        os.remove(requirements_path)


@task(
    aliases=['cc'],
    help={
        'complex': 'filter results to show only potentially complex functions (B+)',
    },
)
def cyclomatic_complexity(ctx, complex_=False):
    """Analise code Cyclomatic Complexity using radon."""
    # Run Cyclomatic Complexity
    cmd = 'radon cc -s -a'
    if complex_:
        cmd += ' -nb'
    ctx.run(f'{cmd} groupage', pty=True)


@task(reformat, lint, tests, safety, aliases=['ci'])
def commit(ctx, amend=False):
    """Run all pre-commit commands and then commit staged changes."""
    cmd = ['git', 'commit']
    if amend:
        cmd.append('--amend')

    ctx.run(' '.join(cmd), pty=True)


def docs_venv(ctx: Context) -> None:
    """Ensure venv for the docs."""
    if not Path('tasks.py').exists():
        raise Exit("You can only run this command from the project's root directory", code=1)

    if Path('docs/.venv/bin/python').exists():
        return

    print('Creating docs venv...')
    with ctx.cd('docs'):
        ctx.run('python -m venv .venv')
        print('Installing dependencies...')
        with ctx.prefix('source .venv/bin/activate'):
            ctx.run('poetry install --no-ansi --no-root')


@contextmanager
def docs_context(ctx: Context) -> typing.Iterator[None]:
    """Context manager to do things in the docs dir with the proper virtualenv."""
    docs_venv(ctx)

    with ctx.cd('docs'):
        with ctx.prefix('source .venv/bin/activate'):
            yield


@task(
    help={
        'build': 'build the docs instead of serving them',
        'verbose': 'enable verbose output',
    },
)
def docs(ctx, build=False, verbose=False):
    """Serve the docs using mkdocs, alternatively building them."""
    args = ['mkdocs']

    if verbose:
        args.append('--verbose')

    if build:
        args.extend(['build', '--clean'])
    else:
        args.append('serve')

    with docs_context(ctx):
        ctx.run(' '.join(args))


@task(
    help={'update': 'update dependencies first'},
    aliases=['docs-reqs'],
)
def docs_requirements(ctx, update=False):
    """Create docs requirements using poetry (overwriting existing one, if any).

    Additionally, if `update` is True then update dependencies first.
    """
    with docs_context(ctx):
        if update:
            print('Updating docs dependencies...')
            ctx.run('poetry install --no-ansi --remove-untracked')
            ctx.run('poetry update --no-ansi')

        print('Exporting docs requirements to readthedocs.requirements.txt...')
        ctx.run('poetry export -f requirements.txt -o readthedocs.requirements.txt')


@task(reformat, lint, tests, safety)
def check(_):
    """Run all checks."""
